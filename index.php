
<!DOCTYPE html>
<html lang="en">
	
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="This is Maxime Paul's Online Domain.  
										  Learn about me, what I do, and how we can work together.  More is coming as 
										  I continue to build it.  Grow with me.">
		<meta name="keywords" content="Maxime Paul, Resume, Innovator, Education, 
									   Technology, Engineering, Empowerment, Creativity, Leadership" />
		<meta name="author" content="Maxime Paul">
		<title>Maxime Paul's Portfolio -- Growing more everyday</title>
		<link href="css/max-portfolio.css" rel="stylesheet">
		<link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
		<link href="css/magnific-popup.css" rel="stylesheet">
	</head>
	
	<body>
		<!-- Side Menu -->	<a id="menu-toggle" href="#" class="btn btn-warning btn-lg toggle"><i class="fa fa-reorder"></i></a>
		
		<div id="sidebar-wrapper">
			<ul class="sidebar-nav">	<a id="menu-close" href="#" class="btn btn-warning btn-lg pull-right toggle"><i class="fa fa-times"></i></a>
				
				<li class="sidebar-brand"><a href="http://maximepaul.com">Maxime Paul</a>
				</li>
				<li><a href="#top">Home</a>
				</li>
				<li><a href="#about">About</a>
				</li>
				<li><a href="#portfolio">Portfolio</a>
				</li>
				<li><a href="#skills">Skills</a>
				</li>
				<li><a href="#contact">Contact</a>
				</li>
				<li><a href="http://www.maximepaul.com/colearninglab.html">Co-Learning Lab</a>
				</li>
			</ul>
		</div>
		<!-- /Side Menu -->
		<!-- Full Page Image Header Area -->
		<div id="top" class="header">
			<div class="vert-text">
				<h1>Maxime Paul's Portfolio</h1>
				
				<h3>I solve problems and design exquisite user experiences with or without technology.  
					All in an attempt to provide the underserved with opportunities that they may not receive otherwise.</h3>
				
				<br>
				<br>
				<h4>Sign up to my new education group: DC's Co-Learning Lab</h4>
				
				<lead>Building your Entreprenuerial Brand.</lead>
				<br>
				<!--<div class=" row">
<div class = "col-md-6">
<form class="form form-horizontal" role="form" id="edSignup" method='post' action=''>
<div class="form-group">   
<label for="user_ed_email">Education 3.0</label>
<button type="button" class="btn btn-link btn-s" data-toggle="popover" data-placement="top" title="A new supradiciplinary curriculum that provides students with the skills to excel in today's world. 
We also redefine pedagogy that creates an environment that cultivates students for current and future success. 
Contact me or sign up for information.">more...</button> 
</div>
<div class="form-group"> 
<div class="col-xs-offset-2 col-xs-8">               
<input type="text" class="form-control" id="user_ed_name" name="user_ed_name" placeholder="Name">
<input type="text" class="form-control" id="user_ed_email" name="user_ed_email" placeholder="Email">
</div>
</div>
<div class="form-group">
<button type="submit" class="btn btn-warning btn-lg" id="edSend">Send</button>
</div>
</form>
</div>
<div class="col-md-6">
<form class="form-horizontal" role="form" id="poliSignup" method='post' action=''>
<div class="form-group">
<label for="user_poli_email">Community Politics</label> 
<button type="button" class="btn btn-link btn-s" data-toggle="popover" data-placement="top" title="A tool that allows the public to be more informed and engaged in complex politics at all levels. 
This tool allows people to be informed and decide without opinion or deciet from larger, self-serving entities that taint the truth. 
Contact me or sign up for information.">more...</button> 
</div>
<div class="form-group">
<div class="col-xs-offset-2 col-xs-8">
<input type="text" class="form-control" id="user_poli_name" name="user_poli_name" placeholder="Name">
<input type="text" class="form-control" id="user_poli_email" name="user_poli_email" placeholder="Email">
</div>
</div>
<div class="form-group">
<button type="submit" class="btn btn-warning btn-lg" id="poliSend">Send</button>
</div>
</form>
</div>
</div>-->
				<div class="container">
					<div class="row">
						<div class="skill-item">
							<div class='col-md-3 text-center btn'>
								<p class='question vert-text'>Ever wanted to know how these startups got to where they are?</p>
							</div>
							<div class='col-md-3 text-center btn'>
								<p class='question vert-text'>Ever wanted to work at a startup?</p>
							</div>
							<div class='col-md-3 text-center btn'>
								<p class='question vert-text'>Do you want to gain entrepreneurial skills?</p>
							</div>
							<div class='col-md-3 text-center btn'>
								<p class='question vert-text'>Ever wanted to create your own business?</p>
							</div>
							<br>
							<br>
							<div class='col-md-3 text-center btn'>
								<p class='question vert-text'>Do you want to learn how to truly learn on your own?</p>
							</div>
							<div class='col-md-3 text-center btn'>
								<p class='question vert-text'>Do you know how to create products that users love?</p>
							</div>
							<div class='col-md-3 text-center btn'>
								<p class='question vert-text'>Do you want to grow your brand online and with some skills you are truly passionate about?</p>
							</div>
							<div class='col-md-3 text-center btn'>
								<p class='question vert-text'>Do you want to gain some innovative, modern skills that you can implement into your current work (intrapreneurship)?</p>
							</div>
						</div>
					</div>
				</div>	
				<br>
				<a href="http://www.meetup.com/DC-Co-Learning-Lab-Building-your-Entrepreneurial-Brand/" target="_blank" class="btn btn-warning btn-lg">DC Co-Learning Lab Meetup</a>
				<a href="http://www.maximepaul.com/colearninglab.html" class="btn btn-warning btn-lg">Co-Learning Lab Surveys</a>
				<br>
				<div class="fb-share-button" data-href="http://www.meetup.com/DC-Co-Learning-Lab-Building-your-Entrepreneurial-Brand/" data-width="150" data-type="button"></div>	<a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.meetup.com/DC-Co-Learning-Lab-Building-your-Entrepreneurial-Brand/" data-via="maxap" data-related="cultivatedsense" data-lang="en" data-count="none">Tweet</a>
				
				<script type="IN/Share" data-url="http://www.meetup.com/DC-Co-Learning-Lab-Building-your-Entrepreneurial-Brand/"></script>
				<div class="alert alert-success col-xs-offset-2 col-xs-8" id="shout">
					<p></p>
				</div>
				<br>
				<br>
				<br>
				<h3>Scroll down to learn more!</h3>
				
				<!-- <svg width="640" height="300" xmlns="http://www.w3.org/2000/svg">
<g>
<title>Layer 1</title>
<path transform="rotate(90 323,65.26754760742192) " id="svg_1" d="m329.96399,57.07825c5.2511,0 10.50323,0 15.75543,0c0,5.45941 0,10.91925 0,16.37863c-5.2522,0 -10.50433,0 -15.75543,0c0,-5.45938 0,-10.91922 0,-16.37863zm-22.75873,0c5.83481,0 11.67072,0 17.50647,0c0,5.45941 0,10.91925 0,16.37863c-5.83575,0 -11.67166,0 -17.50647,0c0,-5.45938 0,-10.91922 0,-16.37863zm-21.00763,0c5.83456,0 11.67053,0 17.50653,0c0,5.45941 0,10.91925 0,16.37863c-5.836,0 -11.67197,0 -17.50653,0c0,-5.45938 0,-10.91922 0,-16.37863zm-21.0076,0c5.83472,0 11.6705,0 17.50653,0c0,5.45941 0,10.91925 0,16.37863c-5.83603,0 -11.67181,0 -17.50653,0c0,-5.45938 0,-10.91922 0,-16.37863zm64.77396,33.77646c5.54288,-0.36972 11.08698,0.2612 16.63098,-0.10843c0.35498,-5.76331 0.19061,-11.52606 0.54681,-17.2894c5.18787,0 10.89624,0 16.08417,0c0,-5.45938 0,-10.91922 0,-16.37863c-5.25238,0 -10.50439,0 -15.75549,0c0,-6.06677 0,-12.13297 0,-18.19901c-5.83597,0 -11.67188,0 -17.50647,0c0,-5.45996 0,-10.91931 0,-16.37924c5.83459,0 11.6705,0 17.50647,0c0,5.45993 0,10.91928 0,16.37924c5.18799,0 10.89633,-0.27057 16.08432,-0.27057c0.35483,5.76266 0.19058,11.79663 0.54663,17.55942c5.54279,0.3696 11.16544,-0.03168 16.70859,0.33862c0,5.39313 -0.07751,11.5571 -0.07751,16.95016c-5.83588,0 -11.67188,0 -17.50653,0c0,6.06677 0,12.1329 0,18.19901c-5.25238,0 -10.50439,0 -15.75549,0c0,5.45996 0,10.91928 0,16.37921c-5.83597,0 -11.67188,0 -17.50647,0c0,-5.3931 0,-11.78735 0,-17.18039l0,0z" stroke-width="5" stroke="#ffffff" fill="#ff7f00"/>
</g>
</svg>     -->
			</div>
		</div>
		<!-- /Full Page Image Header Area -->
		<!-- Intro -->
		<div id="about" class="intro">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-md-offset-3 text-center">
						<h2>About Me</h2>
						
						<hr>
						<p class="lead">My name is pronounced MACK-seem:</p>
						<audio id="player2" style="width: 100%;" src="pronounce.mp3" type="audio/mp3" controls="controls"></audio>
						<script>
							$('audio,video').mediaelementplayer({
								audioWidth: 280,
								enableAutosize: true
							});
						</script>
						<br>
						<p>I could write a lot here about my background, what I've done, and what I can do, but I'll make it simple. I'm a full-stack hacker who is working to combine all of my polymathic skills and experiences to create innovative, user-centered solutions. I am currently working as an IT consultant on all areas of our projects. In addition, I am developing my own education consulting and creative technology firm that specializes in helping underserved communities.</p>
					</div>
				</div>
			</div>
		</div>
		<!-- /Intro -->
		<!-- Portfolio -->
		<div id="portfolio" class="portfolio">
			<div class="container">
				<div class="row">
					<div class="col-md-4 col-md-offset-4 text-center">
						<h2>My Projects</h2>
						
						<hr>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4 text-center">
						<div class="portfolio-item">	<i class="portfolio-icon fa fa-cogs"></i>
							
							<h4>Past Projects</h4>
							
							<p class="lead">Projects I've already completed.</p>	<a href="dox/ELEC 327 Shirts.pdf" class="btn btn-warning btn-xs">Engineering T-shirt Design</a>
							<a href="dox/Senior Design Initial Presentation.pdf" class="btn btn-warning btn-xs">Initial Senior Design (Gesture-based Home Media)</a>
							<a href="dox/Senior Design Poster.pdf" class="btn btn-warning btn-xs">Senior Design Poster(Power-to-the-point)</a>
							<a href="dox/Senior Design Final Presentation.pdf" class="btn btn-warning btn-xs">Senior Design Pres.(Power-to-the-point)</a>
							<a href="dox/Pocket Mail.pdf" class="btn btn-warning btn-xs">USPS Mobile Application (Pocket Mail)</a>
							<a href="http://prezi.com/4xfowjkgqsc8/" class="btn btn-warning btn-xs">STEM Education Presentation</a>
							
							<br>
							<p class="label label-default">Digital Graphic Design</p>
							<p class="label label-default">Junior Design Project (Music Synthesizer)</p>
						</div>
					</div>
					<div class="col-md-4 text-center">
						<div class="portfolio-item">	<i class="portfolio-icon fa fa-wrench"></i>
							
							<h4>Current Projects</h4>
							
							<p class="lead">Projects I'm currently working on.</p>	
                                                        <a href="http://www.cultivatedsense.com" class="btn btn-warning btn-s">Cultivated Sense (Web Design project)</a>
                                                       <a href="https://github.com/Maxap23" class="btn btn-warning btn-xs">Coding (Github)</a>
							<a href="dox/Urban_Mind_Overview_1.0.pdf" class="btn btn-warning btn-xs">Urban Mind 1.0 (21st Century Education)</a>
							<a href="dox/UM Overview 2.0.pdf" class="btn btn-warning btn-xs">Urban Mind 2.0 (Education Teachnology)</a>
							
							<p class="label label-default">What's Popular (Mobile Game)</p>
							<p class="label label-default">Technology Business Consulting</p>
							<p class="label label-default">Education Consulting</p>
							<p class="label label-default">Web Design</p>
						</div>
					</div>
					<div class="col-md-4 text-center">
						<div class="portfolio-item">	<i class="portfolio-icon fa fa-rocket"></i>
							
							<h4>Future Projects</h4>
							
							<p class="lead">Projects I have in the pipeline.</p>
							<p class="label label-default">Sustainable Community Water Fountain</p>
							<p class="label label-default">Targeted Food Rescue Program</p>
							<p class="label label-default">The Urban Explorers Society (TUES)</p>
							<p class="label label-default">Community Politics (Politics, on YOUR terms)</p>
							<p class="label label-default">Payphone NOW! (21st Century Payphone)</p>
							<p class="label label-default">New Ideas Everyday!!!</p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4 col-md-offset-4 text-center">
							<hr>	<a href="#contact" class="btn btn-warning btn-lg">Contact to Learn More</a>
							
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /Portfolio -->
		<!-- Callout -->
		<div class="callout">
			<div class="vert-text">
				<h1>What skills does it take to do all of that?</h1>
				
			</div>
		</div>
		<!-- /Callout -->
		<!-- Skills -->
		<div id="skills" class="skills">
			<div class="container">
				<div class="row">
					<div class="col-md-4 col-md-offset-4 text-center">
						<h2>My Skills</h2>
						
						<hr>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4 text-center">
						<div class="skill-item">
							<img class="img-skill img-responsive" src="img/tech2.jpg">
						</a>
						<h4>Technology</h4> 	<a href="#popup" class="btn btn-warning btn-xs popup-with-move-anim">Developer(Click to see languages)</a> 
						<p class="label label-default">Software Engineer</p>
						<p class="label label-default">Software Development Lifecycle</p>
						<p class="label label-default">Mobile Development</p>
						<p class="label label-default">Information Architect</p>
						<p class="label label-default">Search Engine Marketing</p>
						<p class="label label-default">Social Media Marketing</p>
						<p class="label label-default">Agile Development (Product Owner)</p>
						<p class="label label-default">Data Analysis</p>
					</div>
				</div>
				<div class="col-md-4 text-center">
					<div class="skill-item">
						<img class="img-skill img-responsive" src="img/design3.jpg">
					</a>
					<h4>Design</h4>
					
					<p class="label label-default">UX Design</p>
					<p class="label label-default">Object Oriented Design</p>
					<p class="label label-default">User Centered Design</p>
					<p class="label label-default">Curriculum Design</p>
					<p class="label label-default">Business Design</p>
					<p class="label label-default">Product Design</p>
					<p class="label label-default">Mobile Design</p>
					<p class="label label-default">Ideation</p>
					<p class="label label-default">Design Research</p>
					<p class="label label-default">Creative Problem-Solving</p>
				</div>
			</div>
			<div class="col-md-4 text-center">
				<div class="skill-item">
					<img class="img-skill img-responsive" src="img/leader1.jpg">
				</a>
				<h4>Leadership</h4>
				
				<p class="label label-default">Creative Strategy</p>
				<p class="label label-default">Product Management</p>
				<p class="label label-default">Clear Written &#38; Oral Communication</p>
				<p class="label label-default">Planning</p>
				<p class="label label-default">High Emotional Intelligence</p>
				<p class="label label-default">Cross-Functional Team Leadership</p>
				<p class="label label-default">Entrepreneurship</p>
				<p class="label label-default">Lean Start-Up Methodology</p>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4 text-center">
			<div class="skill-item">
				<img class="img-skill img-responsive" src="img/business1.jpg">
			</a>
			<h4>Business</h4> 
			<p class="label label-default">Marketing</p>
			<p class="label label-default">Research and Analysis</p>
			<p class="label label-default">Project Management</p>
			<p class="label label-default">Financial Analysis</p>
			<p class="label label-default">Connector, Maven, &#38; Salesperson</p>
			<p class="label label-default">Process Improvement</p>
		</div>
	</div>
	<div class="col-md-4 text-center">
		<div class="skill-item">
			<img class="img-skill img-responsive" src="img/education2.png">
		</a>
		<h4>Education</h4>
		
		<p class="label label-default">ADDIE Model</p>
		<p class="label label-default">K-Adult Instruction</p>
		<p class="label label-default">Blended Learning</p>
		<p class="label label-default">Project-Based Learning</p>
		<p class="label label-default">Heutagogy, Andragogy, &#38; Pedagogy</p>
		<p class="label label-default">STEM Education</p>
	</div>
</div>
<div class="col-md-4 text-center">
	<div class="skill-item">
		<img class="img-skill img-responsive" src="img/misc1.jpg">
	</a>
	<h4>Miscellaneous</h4>
	
	<p class="label label-default">Lateral Thinking</p>
	<p class="label label-default">Rapid Skill Acquisition</p>
	<p class="label label-default">Autodidactism</p>
	<p class="label label-default">Resourcefullness</p>
	<p class="label label-default">Curation</p>
	<p class="label label-default">Name it and I can do or learn it.</p>
</div>
</div>
<div id=popup class="zoom-anim-dialog mfp-hide">
	<h3><span class="label label-warning">JavaScript, JQuery, JAVA</span></h3>
	
	<h3><span class="label label-warning">HTML(HTML5), CSS(SCSS,LESS)</span></h3>
	
	<h3><span class="label label-warning">SQL(MySQL,SQL Server,PostgreSQL, PL/SQL)</span></h3>
	
	<h3><span class="label label-warning">PHP</span></h3>
	
	<h3><span class="label label-warning">XML</span></h3> 
	<h3><span class="label label-warning">Ruby(Rails)</span></h3>
	
	<h3><span class="label label-warning">Python</span></h3>
	
	<h3><span class="label label-warning">C, C++</span></h3> 
	<h3><span class="label label-warning">Visual BASIC</span></h3>
	
	<h3><span class="label label-warning">VB.Net</span></h3>
	
	<h3><span class="label label-warning">Matlab</span></h3> 
	<h3><span class="label label-warning">Verilog</span></h3>
	
</div>
</div>
</div>
</div>
<!-- /Skills -->
<!-- Contact -->
<div id="contact" class="contact">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3 text-center">
				<h2>Contact Me</h2>
				
				<hr>
				<p>Please contact me if you are interested in learning more about me, my projects, my skills, or have a position that may take advantage of my vast array of skills.</p>
				<ul class="list-inline">
					<li><a href="http://www.linkedin.com/in/maximepaul"><i class="fa fa-linkedin fa-3x social"></i></a>
					</li>
					<li><a href="http://www.twitter.com/maxap"><i class="fa fa-twitter fa-3x social"></i></a>
					</li>
					<li><a href="mailto:maxime.a.paul@gmail.com"><i class="fa fa-envelope fa-3x social"></i></a>
					</li>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 col-md-offset-4 text-center">
				<hr>	<a href="dox/Maxime Paul WebRes.pdf" class="btn btn-warning btn-lg">Resume</a>
				
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 col-md-offset-4 text-center">
				<h3>
					Share this site!
				</h3>
				
				<div class="fb-share-button" data-href="http://www.maximepaul.com" data-width="150" data-type="button"></div>	<a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.maximepaul.com" data-via="maxap" data-related="cultivatedsense" data-lang="en" data-count="none">Tweet</a>
				
				<script type="IN/Share" data-url="http://www.maximepaul.com"></script>
			</div>
		</div>
	</div>
</div>
<!-- /Contact -->
<!-- Footer -->
<footer>
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3 text-center">
				<div class="top-scroll">
					<h3>Return to the top</h3>
					<a href="#top"><i class="fa fa-arrow-circle-up scroll fa-4x"></i></a>
					
				</div>
				<hr>
				<p>Copyright &copy; Maxime Paul 2015</p>
			</div>
		</div>
	</div>
</footer>
<!-- /Footer -->
<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<!--<script src="js/mp.js"></script>       -->
<script>
	$("#menu-close").click(function(e) {
		e.preventDefault();
		$("#sidebar-wrapper").toggleClass("active");
	});
</script>
<script>
	$("#menu-toggle").click(function(e) {
		e.preventDefault();
		$("#sidebar-wrapper").toggleClass("active");
	});
</script>
<script>
	$('.popup-with-move-anim').magnificPopup({
		type: 'inline',
		
		fixedContentPos: false,
		fixedBgPos: true,
		
		overflowY: 'auto',
		
		closeBtnInside: true,
		preloader: false,
		
		midClick: true,
		removalDelay: 300,
		mainClass: 'my-mfp-slide-bottom'
	});
</script>
<script>
	$(function() {
		$('a[href*=#]:not([href=#])').click(function() {
			if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') || location.hostname == this.hostname) {
				
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top
					}, 1000);
					return false;
				}
			}
		});
	});
</script>
<script>
	$('[data-toggle="popover"]').popover({
		trigger: 'hover',
		'placement': 'top'
	});
</script>/*
<script>
	$("#edSend").on("click", function(e) {
		e.preventDefault();
		$('#shout').show();
		$("#shout").append("<span>Thanks for your interest in Education 3.0 you will be contacted shortly!<br>
						   Please also sign up for the Community Politics project if interested.<br>
						   Otherwise scrolldown to view more.<span>");
						   });
		
		$("#poliSend").click(function(e) {
			e.preventDefault();
			$("#shout").show();
			$("#shout").append("<span>Thanks for your interest in Community Politics you will be contacted shortly!<br>
							   Please also sign up for the Education 3.0 project if interested.<br>
							   Otherwise scrolldown to view more.<span>");
							   });
</script>*/
<div id="fb-root"></div>
<script>
	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s);
		js.id = id;
		js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
</script>
<script>
	! function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (!d.getElementById(id)) {
			js = d.createElement(s);
			js.id = id;
			js.src = "https://platform.twitter.com/widgets.js";
			fjs.parentNode.insertBefore(js, fjs);
		}
	}(document, "script", "twitter-wjs");
</script>
<script src="//platform.linkedin.com/in.js" type="text/javascript">
	lang: en_US
</script>

<script>
	function() {
		var maxHeight = 0;
		$(".question").each(function() {
			if ($(this).outerHeight() > maxHeight) {
				maxHeight = $(this).outerHeight();
			}
		}).height(maxHeight);
	}
</script>
<script>
	(function(i, s, o, g, r, a, m) {
		i['GoogleAnalyticsObject'] = r;
		i[r] = i[r] || function() {
			(i[r].q = i[r].q || []).push(arguments)
		}, i[r].l = 1 * new Date();
		a = s.createElement(o),
			m = s.getElementsByTagName(o)[0];
		a.async = 1;
		a.src = g;
		m.parentNode.insertBefore(a, m)
	})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
	
	ga('create', 'UA-43061793-1', 'maximepaul.com');
	ga('send', 'pageview');
</script>
</body>

</html>