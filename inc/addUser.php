<?php
include 'db-connect.php';

if(isset($_POST['user_ed_email']) && isset($_POST['user_ed_name'])){
 $name = $mysqli->quote($_POST['user_ed_name']);
 $email = $mysqli->quote($_POST['user_ed_email']);
 $sql = "INSERT INTO ed_email (name,email) VALUES (:name,:email)";

try {
  $stmt = $mysqli->prepare($sql);
  $stmt->execute(array(':name'=>$name, ':email'=>$email));
echo "Successfully inserted".$stmt->rowcount();
} catch(PDOException $e) {
  trigger_error('Wrong SQL: '.$sql.' Error: '. $e->getMessage(), E_USER_ERROR);
}

// Close connection
$mysqli = null;
//header('Location: file:///C:/xampp/htdocs/stylish-portfolio/index.php');
 
}
elseif (isset($_POST['user_poli_email']) && isset($_POST['user_poli_name'])){
 $name = $mysqli->quote($_POST['user_poli_name']);
 $email = $mysqli->quote($_POST['user_poli_email']);
 $sql = "INSERT INTO poli_email (name,email) VALUES (:name,:email)";
 
try {
  $stmt = $mysqli->prepare($sql);
  $stmt->execute(array(':name'=>$name, ':email'=>$email));
echo "Successfully inserted".$stmt->rowcount();
} catch(PDOException $e) {
  trigger_error('Wrong SQL: ' . $sql . ' Error: ' . $e->getMessage(), E_USER_ERROR);
}

// Close connection
$mysqli = null;
//header('Location: file:///C:/xampp/htdocs/stylish-portfolio/index.php');
}

?>