
        $("#menu-close").click(function(e) {
            e.preventDefault();
            $("#sidebar-wrapper").toggleClass("active");
        });
    
        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#sidebar-wrapper").toggleClass("active");
        });
 
        $('.popup-with-move-anim').magnificPopup({
      		type: 'inline',
      
      		fixedContentPos: false,
      		fixedBgPos: true,
      
      		overflowY: 'auto',
      
      		closeBtnInside: true,
      		preloader: false,
      		
      		midClick: true,
      		removalDelay: 300,
      		mainClass: 'my-mfp-slide-bottom'
      	});
         
      $(function() {
        $('a[href*=#]:not([href=#])').click(function() {
          if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') 
            || location.hostname == this.hostname) {

            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
              $('html,body').animate({
                scrollTop: target.offset().top
              }, 1000);
              return false;
            }
          }
        });
      });

      $('[data-toggle="popover"]').popover({
          trigger: 'hover',
              'placement': 'top'
      });
      $("#shout").click(function(){
        $("p").append("<span>Thanks for your interest in Education 3.0 you will 
        be contacted shortly!<br>Please also sign up for the Community Politics project if interested)
        $("p").show();
      });